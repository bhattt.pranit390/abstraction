Practical scenarios

The Java Virtual Machine is an abstraction. Your code can use the exact same java methods irrespective of whether your code will run on Linux or Windows or any other OS. Underneath, a lot of things will change depending on the OS, which the user doesn’t need to know.

You can use a simple API call to fetch weather data or stock prices. These API abstract out the internal complexities of how it is implemented.

Software libraries are good examples of abstraction. They tell you the methods you can use without telling you how they are implemented.
========================================================================================================================================================
Summary of Abstraction

It separates the method signature from its implementation.

Closely related classes can separate out common fields/methods into a different class and reuse that class.
================================================================================================================================================
Why is it useful? - To the implementers of the class, it is useful since it provides them with the flexibility to change things as long as the method signatures don’t change. For the users, it keeps them from getting exposed to anything other than the method signatures they need to know, reducing complexity.

How to apply? - Use a smaller set of public methods

What is the drawback if we don’t use abstraction? - Low maintainability because responsibilities are not clearly differentiated. Higher Complexity with larger code bases because many objects interact with others and it becomes difficult to add functionality without impacting others.
//==================================================================================================================================================================
Language specific notes

In Java, interface classes provides total abstraction and abstract classes provide partial abstraction

In C++, you can use either header files to achieve abstraction or private methods to hide implementation details.

Python interface can be applied using the the Abstract Base Class library
==============================================================================================================================================================
General Abstraction notes

Hides underlying complexity of data from users

Helps avoid repetitive code

Presents only the signature of internal functionality

Gives flexibility to programmers to change the implementation of the abstract behaviour
=======================================================================================================================================================================
Difference between Encapsulation and Abstraction

Abstraction is a natural extension of Encapsulation. Encapsulation recommends that you create entities/objects which contain closely related fields and functionality/methods. Abstraction further recommends that the entities expose only their method signatures to external users who shouldn’t need to worry about the underlying implementation.

Encapsulation - hide data. Abstraction - hide complexity.
===================================================================================================




Code Structure
Fetch the code from this git repo as specified in the Setup & Getting Started task if you haven’t already.


Let’s look at how our current implementation is laid out & what the different project files are. All project files are under the src/main/java/com/example/abstraction/ directory. We have version 1 inside the base_code/ directory. Files we have are:

config/ConfigManager.java - has utility methods for reading & writing data to the config file in JSON format. Multiple teams will use the methods offered by this Config Manager.

InventoryHardware.java - contains methods required to read/write configuration related to the MySQL database of the inventory team, to JSON files.

LogisticsHardware.java - contains methods required to read/write configuration related to the MySQL database & REST server used by the logistics team, to JSON files.

VendorHardware.java - contains methods required to read/write configuration related to the MySQL database & resolv configuration for DNS resolver of the vendor management team, to JSON files.

Main.java - calls the respective methods of products to demonstrate the writing and reading of their configuration files.

config_files/ - this directory will be the location where all the configuration files are stored. It may initially not be present and will be created and populated with files when you run the application.

dto/ - contains classes like MySqlConfig, ResolvConfig to denote the Java objects each configuration creates.

Let’s go a bit deeper to understand what’s happening.


config/ConfigManager.java contains ConfigManager class which implements two methods readConfig() & writeConfig() for interacting with the config files. The files will be stored inside the base_code/config_files directory. Currently, we’re going with the JSON format to store our configurations as it is pretty common & easy to deal with.


We have many product teams that have to deal with multiple types of configuration. All of them have similar implementations that utilize the ConfigManager class methods, for reading/storing specific configuration from/to disk as JSON. For example, the Inventory team’s config handler is InventoryHardware.java




if we see, the storeMySqlConfig() method stores the MySQL configuration in a file named inventory_mysql_config.json. Can you quickly check the methods that the Logistics & the Vendor Management teams have?


To demonstrate our implementation, we’ll use the Main.java file. It calls the methods used by all our teams. In the real product, the same methods may be invoked from a different place. Let’s see how the inventory team’s configurations are called using the methods in InventoryHardware.java





As you can see, the configuration is first added as fields of a MySqlConfig object and then passed to the storeMySqlConfig() method we saw earlier to work its magic!


The Main.java file has code to interact with other methods as well. Try running the file by right-clicking inside the file and selecting Run. You will see the base_code/config_files created & the configuration files of all of our teams populated inside it.